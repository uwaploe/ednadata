// Package ednadata parses data files from the eDNA sampler
package ednadata

import (
	"archive/tar"
	"compress/gzip"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/spf13/afero"
)

func filenameOk(name string) bool {
	if len(name) != 27 {
		return false
	}
	if !strings.HasPrefix(name, "edna_") {
		return false
	}
	if !strings.HasSuffix(name, ".tar.gz") {
		return false
	}

	return true
}

// Deployment contains the data from a single eDNA Sampler deployment
type Deployment struct {
	ID       string
	pathname string
	dstFs    afero.Fs
}

// ReadDeployment opens a new Deployment archive file for reading
func ReadDeployment(pathname string) (*Deployment, error) {
	filename := filepath.Base(pathname)
	if !filenameOk(filename) {
		return nil, fmt.Errorf("Invalid filename: %q", filename)
	}

	// File name is edna_YYYYmmddTHHMMSS.tar.gz
	return &Deployment{ID: filename[5:20], pathname: pathname}, nil
}

// Summary returns the deployment results.
func (d *Deployment) Summary() (Result, error) {
	r := Result{ID: d.ID}
	f, err := os.Open(d.pathname)
	if err != nil {
		return r, err
	}
	defer f.Close()

	zr, err := gzip.NewReader(f)
	if err != nil {
		return r, err
	}

	tr := tar.NewReader(zr)
	dfile := "edna_" + r.ID + ".ndjson"
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return r, err
		}
		if strings.HasSuffix(hdr.Name, dfile) {
			r.Samples, err = getSamples(tr)
			break
		}
	}

	return r, err
}

// Csv extracts the deployment data from a compressed TAR archive file
// specified by pathname to a set of CSV files. The files will be stored in
// the directory dir or the current directory if dir is the empty string.
func (d *Deployment) Csv(dir string) error {
	f, err := os.Open(d.pathname)
	if err != nil {
		return err
	}
	defer f.Close()

	zr, err := gzip.NewReader(f)
	if err != nil {
		return err
	}

	tr := tar.NewReader(zr)
	dfile := "edna_" + d.ID + ".ndjson"
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		if strings.HasSuffix(hdr.Name, dfile) {
			return toCsv(tr, dir, d.ID, d.dstFs)
		}
	}

	return errors.New("No data found")
}

// Result contains a summary of the Deployment
type Result struct {
	ID      string   `json:"id"`
	Samples []Sample `json:"samples"`
}

func getSamples(rdr io.Reader) ([]Sample, error) {
	dec := json.NewDecoder(rdr)
	samples := make([]Sample, 0)
	for {
		var (
			rec    Record
			sample Sample
		)

		err := dec.Decode(&rec)
		if err == io.EOF {
			break
		}
		if err != nil {
			return samples, err
		}
		if strings.HasPrefix(rec.Event, "result.") {
			rec.Scan(&sample)
			sample.Index, _ = strconv.Atoi(rec.Event[7:])
			samples = append(samples, sample)
		}
	}

	return samples, nil
}

func toCsv(rdr io.Reader, dir, id string, fs afero.Fs) error {
	if fs == nil {
		fs = afero.NewOsFs()
	}

	files := make(map[string]afero.File)
	headers := map[string]string{
		"sample":  "time,sample,elapsed,amount,pr,pr_ok,depth",
		"result":  "time,sample,elapsed,vwater,vethanol,overpressure,deptherror",
		"depth":   "time,depth",
		"battery": "time,battery,voltage,current,soc",
	}

	var (
		err  error
		path string
	)

	// Create the CSV files and write the header
	for _, rtype := range []string{"sample", "result", "depth", "battery"} {
		path = filepath.Join(dir, fmt.Sprintf("%s_%s.csv", rtype, id))
		f, err := fs.Create(path)
		if err != nil {
			return fmt.Errorf("open %q: %w", path, err)
		}
		files[rtype] = f
		f.Write([]byte(headers[rtype]))
		f.Write([]byte("\n"))
		defer f.Close()
	}
	files["battery-0"] = files["battery"]
	files["battery-1"] = files["battery"]

	// Create the metadata file
	path = filepath.Join(dir, fmt.Sprintf("metadata_%s.txt", id))
	files["metadata"], err = fs.Create(path)
	if err != nil {
		return fmt.Errorf("open %q: %w", path, err)
	}
	defer files["metadata"].Close()

	dec := json.NewDecoder(rdr)

	// Write each record to the appropriate file.
	for {
		rec := Record{}
		err := dec.Decode(&rec)
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		parts := strings.Split(rec.Event, ".")
		if f, ok := files[parts[0]]; ok {
			b, _ := rec.MarshalText()
			f.Write(b)
			f.Write([]byte("\n"))
		}
	}

	return nil
}
