package ednadata

import (
	"encoding/json"
	"fmt"
	"path/filepath"
	"testing"

	"github.com/spf13/afero"
)

func TestDeploymentSummary(t *testing.T) {
	id := "20201107T234941"
	d, err := ReadDeployment(filepath.Join("testdata",
		"edna_"+id+".tar.gz"))
	if err != nil {
		t.Fatal(err)
	}

	r, err := d.Summary()
	if err != nil {
		t.Fatal(err)
	}

	if r.ID != id {
		t.Errorf("Bad ID; expected %q, got %q", id, r.ID)
	}

	if len(r.Samples) != 3 {
		t.Errorf("Bad sample count; expected 3, got %d", len(r.Samples))
	}

}

func TestCsvFormat(t *testing.T) {
	table := []struct {
		in, out string
	}{
		{
			in:  `{"t": "2021-01-20T02:35:33.990+00:00", "event": "depth", "data": {"depth": 0.476}}`,
			out: `"2021-01-20T02:35:33.990+00:00",0.476`,
		},
		{
			in:  `{"t": "2021-01-20T02:35:40.028+00:00", "event": "sample.1", "data": {"elapsed": 0.175, "amount": 0.0, "pr": -3.816, "pr_ok": true, "depth": 6.837}}`,
			out: `"2021-01-20T02:35:40.028+00:00","1",0.175,0.000,-3.816,true,6.837`,
		},
		{
			in:  `{"t": "2021-01-20T02:36:53.354+00:00", "event": "result.1", "data": {"elapsed": 65.139, "vwater": 0.26, "vethanol": 0.017, "overpressure": false, "deptherror": false}}`,
			out: `"2021-01-20T02:36:53.354+00:00","1",65.139,0.260,0.017,false,false`,
		},
		{
			in:  `{"t": "2021-01-20T02:36:39.927+00:00", "event": "battery-0", "data": {"v": 0.0, "a": 0.0, "soc": 0}}`,
			out: `"2021-01-20T02:36:39.927+00:00","0",0.000,0.000,0`,
		},
		{
			in:  `{"t": "2021-01-20T02:36:39.927+00:00", "event": "battery-1", "data": {"v": 0.0, "a": 0.0, "soc": 0}}`,
			out: `"2021-01-20T02:36:39.927+00:00","1",0.000,0.000,0`,
		},
		{
			in:  `{"t": "2021-01-20T02:35:33.990+00:00", "event": "metadata", "data": ["cast", "test-001", "cruise", "Hake Survey 2021"]}`,
			out: "cast=test-001\ncruise=Hake Survey 2021\n",
		},
	}

	for _, e := range table {
		rec := Record{}
		err := json.Unmarshal([]byte(e.in), &rec)
		if err != nil {
			t.Fatal(err)
		}
		text, err := rec.MarshalText()
		if err != nil {
			t.Fatalf("MarshalText failed: %v", err)
		}

		out := string(text)
		if out != e.out {
			t.Errorf("Output error; expected %q, got %q", e.out, out)
		}
	}

}

func TestCsvExport(t *testing.T) {
	id := "20201107T234941"
	d, err := ReadDeployment(filepath.Join("testdata",
		"edna_"+id+".tar.gz"))
	if err != nil {
		t.Fatal(err)
	}

	expected := make(map[string]bool)
	for _, rtype := range []string{"sample", "result", "depth", "battery"} {
		name := fmt.Sprintf("%s_%s.csv", rtype, id)
		expected[name] = true
	}
	expected[fmt.Sprintf("metadata_%s.txt", id)] = true

	fs := afero.NewMemMapFs()
	fs.MkdirAll("output", 0755)
	d.dstFs = fs
	err = d.Csv("output")
	if err != nil {
		t.Fatal(err)
	}

	listing, err := afero.ReadDir(fs, "output")
	if err != nil {
		t.Fatal(err)
	}

	created := make(map[string]bool)
	for _, fi := range listing {
		if !expected[fi.Name()] {
			t.Errorf("Unexpected file: %q", fi.Name())
		}
		created[fi.Name()] = true
	}

	for k := range expected {
		if !created[k] {
			t.Errorf("Expected file not created: %q", k)
		}
	}

}
