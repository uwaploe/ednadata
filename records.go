package ednadata

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type SampleStatus struct {
	Index   int     `json:"index"`
	Elapsed float32 `json:"elapsed"`
	Amount  float32 `json:"amount"`
	Pr      float32 `json:"pr"`
	PrOk    bool    `json:"pr_ok"`
	Depth   float32 `json:"depth"`
}

func (s SampleStatus) MarshalText() ([]byte, error) {
	text := fmt.Sprintf("\"%d\",%.3f,%.3f,%.3f,%t,%.3f",
		s.Index,
		s.Elapsed,
		s.Amount,
		s.Pr,
		s.PrOk,
		s.Depth)
	return []byte(text), nil
}

type Sample struct {
	Index        int     `json:"index"`
	Elapsed      float32 `json:"elapsed"`
	Vwater       float32 `json:"vwater"`
	Vethanol     float32 `json:"vethanol"`
	Overpressure bool    `json:"overpressure"`
	Deptherror   bool    `json:"deptherror,omitempty"`
}

func (s Sample) MarshalText() ([]byte, error) {
	text := fmt.Sprintf("\"%d\",%.3f,%.3f,%.3f,%t,%t",
		s.Index,
		s.Elapsed,
		s.Vwater,
		s.Vethanol,
		s.Overpressure,
		s.Deptherror)
	return []byte(text), nil
}

type Depth struct {
	Depth float32 `json:"depth"`
}

func (d Depth) MarshalText() ([]byte, error) {
	text := fmt.Sprintf("%.3f", d.Depth)
	return []byte(text), nil
}

type Battery struct {
	Battery int     `json:"battery"`
	V       float32 `json:"v"`
	A       float32 `json:"a"`
	Soc     int     `json:"soc"`
}

func (b Battery) MarshalText() ([]byte, error) {
	text := fmt.Sprintf("\"%d\",%.3f,%.3f,%d",
		b.Battery,
		b.V,
		b.A,
		b.Soc)
	return []byte(text), nil
}

type MetaEntry struct {
	Key, Value string
}

type Metadata []MetaEntry

func (m *Metadata) UnmarshalJSON(b []byte) error {
	s := make([]string, 0)
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	n := len(s)
	if (n & 1) == 1 {
		return fmt.Errorf("Invalid metadata: %q", b)
	}

	*m = make([]MetaEntry, 0, n/2)
	for i := 0; i < n; i += 2 {
		*m = append(*m, MetaEntry{Key: s[i], Value: s[i+1]})
	}

	return nil
}

func (m Metadata) MarshalText() ([]byte, error) {
	b := make([]byte, 0)
	for _, e := range m {
		s := fmt.Sprintf("%s=%s\n", e.Key, e.Value)
		b = append(b, s...)
	}
	return b, nil
}

type Record struct {
	T     time.Time       `json:"t"`
	Event string          `json:"event"`
	Data  json.RawMessage `json:"data"`
}

const timeFmt = "2006-01-02T15:04:05.000-07:00"

func (r Record) MarshalText() ([]byte, error) {
	// Metadata recors are not output as CSV
	if r.Event == "metadata" {
		v := Metadata{}
		r.Scan(&v)
		return v.MarshalText()
	}

	buf := []byte(fmt.Sprintf("%q,", r.T.Format(timeFmt)))
	parts := strings.Split(r.Event, ".")
	switch parts[0] {
	case "sample":
		v := SampleStatus{}
		r.Scan(&v)
		v.Index, _ = strconv.Atoi(parts[1])
		b, _ := v.MarshalText()
		buf = append(buf, b...)
	case "result":
		v := Sample{}
		r.Scan(&v)
		v.Index, _ = strconv.Atoi(parts[1])
		b, _ := v.MarshalText()
		buf = append(buf, b...)
	case "depth":
		v := Depth{}
		r.Scan(&v)
		b, _ := v.MarshalText()
		buf = append(buf, b...)
	case "battery-0":
		v := Battery{}
		r.Scan(&v)
		v.Battery = 0
		b, _ := v.MarshalText()
		buf = append(buf, b...)
	case "battery-1":
		v := Battery{}
		r.Scan(&v)
		v.Battery = 1
		b, _ := v.MarshalText()
		buf = append(buf, b...)
	}

	return buf, nil
}

// Decode the data property into the value pointed at by v.
func (r Record) Scan(v interface{}) error {
	return json.Unmarshal(r.Data, v)
}
